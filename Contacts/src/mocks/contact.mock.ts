import { Injectable, Inject, forwardRef } from '@angular/core';
import { ContactsMock } from './contacts.mock';
import { Contact, ContactField, IContactField } from '@ionic-native/contacts';

@Injectable()
export class ContactMock
{
    contactsMock: ContactsMock;
    displayName: string;
    nickname: string;
    phoneNumbers: ContactFieldMock[];

    constructor(@Inject(forwardRef(() => ContactsMock)) contactsMockRef: ContactsMock, private contact?: Contact)
    {
        this.contactsMock = contactsMockRef;
        if(contact != null)
        {
            this.displayName = contact.displayName;
            this.nickname = contact.nickname;
            let contactFields: ContactFieldMock[] = [];
            if(contact.phoneNumbers != null)
            {
                for(var i = 0; i < contact.phoneNumbers.length; i++)
                {
                    contactFields[i] = new ContactFieldMock(contact.phoneNumbers[i]);
                }
            }
            this.phoneNumbers = contactFields;
        }
        else
        {
            this.displayName = '';
            this.nickname = '';
            this.phoneNumbers = [];
        }
    }

    clone() : ContactMock
    {
        if(!this.contactsMock.platform.is('cordova'))
        {
            let contactMock: ContactMock = new ContactMock(this.contactsMock);
            contactMock = this;
            return contactMock;
        }
        else
        {
            let contactMock: ContactMock = new ContactMock(this.contactsMock, this.contact.clone());
            return contactMock;
        }
    }

    remove() : Promise<any>
    {
        return new Promise((resolve, reject) => {
            for(var i = 0; i < this.contactsMock.contactsFound.length; i++)
            {
                if(this.contactsMock.contactsFound[i] == this)
                {
                    this.contactsMock.contactsFound.splice(i, 1);
                }
            }
            //this.contactsMock.find(this.contactsMock.lastFields, this.contactsMock.lastOptions).then((allContacts) => {
            //    this.contactsMock.contactsFound = allContacts;
            //});

            if(!this.contactsMock.platform.is('cordova'))
            {
                resolve();
            }
            else
            {
                if(this.contact != null)
                {
                    this.contact.remove().then(() => resolve());
                }
            }
        })
    }

    save() : Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.contactsMock.contactsFound.push(this);
            //this.contactsMock.find(this.contactsMock.lastFields, this.contactsMock.lastOptions).then((allContacts) => {
            //    this.contactsMock.contactsFound = allContacts;
            //});

            if(!this.contactsMock.platform.is('cordova'))
            {
                resolve();
            }
            else
            {
                if(this.contact != null)
                {
                    this.contact.displayName = this.displayName;
                    this.contact.nickname = this.nickname;
                    let contactFields: ContactField[] = [];
                    for(var i = 0; i < this.phoneNumbers.length; i++)
                    {
                        contactFields[i] = new ContactField(this.phoneNumbers[i].type,
                            this.phoneNumbers[i].value, this.phoneNumbers[i].pref);
                    }
                    this.contact.phoneNumbers = contactFields;
                    this.contact.save().then(() => resolve());
                }
            }
        })
    }
}

@Injectable()
export class ContactFieldMock
{
    type: string;
    value: string;
    pref: boolean;

    constructor(private contactField?: IContactField)
    {
        if(contactField != null)
        {
            this.type = contactField.type;
            this.value = contactField.value;
            this.pref = contactField.pref;
        }
        else
        {
            this.type = '';
            this.value = '';
            this.pref = true;
        }
    }
}