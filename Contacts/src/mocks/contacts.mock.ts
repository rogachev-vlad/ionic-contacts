import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { ContactMock, ContactFieldMock } from './contact.mock';
import { Contact, Contacts, ContactFieldType, ContactFindOptions } from '@ionic-native/contacts'
import { TestContacts } from '../pages/services/test-contacts';

@Injectable()
export class ContactsMock
{
    contactsFound = [];
    lastFields: ContactFieldTypeMock[] = [];
    lastOptions: IContactFindOptionsMock = {};

    constructor(public platform: Platform, private contacts: Contacts, private testContacts: TestContacts) {}

    find(fields:ContactFieldTypeMock[], options?:IContactFindOptionsMock) : Promise<ContactMock[]>
     {
        return new Promise<ContactMock[]>((resolve, reject) => {
            this.lastFields = fields;
            this.lastOptions = options;

            if(!this.platform.is('cordova'))
            {
                let contactMocks: ContactMock[] = [];
                let testContacts: any[] = this.testContacts.getTestContacts();
                for(var i = 0; i < testContacts.length; i++)
                {
                    let contact: ContactMock = new ContactMock(this);
                    contact.displayName = testContacts[i].displayName;
                    contact.nickname = testContacts[i].nickName;

                    var contactField = new ContactFieldMock();
                    contactField.type = testContacts[i].phoneType;
                    contactField.value = testContacts[i].phoneNumber;
                    contactField.pref = true;

                    var numberSection = [];
                    numberSection.push(contactField);
                    contact.phoneNumbers = numberSection;

                    contactMocks.push(contact);
                }
                this.contactsFound = contactMocks;
                resolve(this.contactsFound);
            }
            else
            {
                let f:ContactFieldType[] = [];
                for(var i = 0; i < fields.length; i++)
                {
                    f.push(fields[i]);
                }

                const opt = new ContactFindOptions();
                opt.filter = options.filter;
                opt.multiple = options.multiple;
                opt.hasPhoneNumber = options.hasPhoneNumber;
                opt.desiredFields = options.desiredFields;

                this.contacts.find(f, opt).then((nativeContacts) => {
                    //console.log('found ' + nativeContacts.length);
                    let contactMocks: ContactMock[] = [];
                    for(var i = 0; i < nativeContacts.length; i++)
                    {
                        //console.log('field mocks ' + contactFieldMocks.length);
                        contactMocks[i] = new ContactMock(this, nativeContacts[i]);
                    }
                    //console.log('prepared');
                    this.contactsFound = contactMocks;
                    resolve(this.contactsFound);
                });
            }

        })
    }

    create() : ContactMock
    {
        if(!this.platform.is('cordova'))
        {
            let contactMock:ContactMock = new ContactMock(this);
            return contactMock;
        }
        else
        {
            let contact: Contact = this.contacts.create();
            let contactMock:ContactMock = new ContactMock(this, contact);

            return contactMock;
        }
    }
}

export type ContactFieldTypeMock = '*' | 'addresses' | 'birthday' | 'categories' | 'country' | 'department' | 'displayName' | 'emails' | 'familyName' | 'formatted' | 'givenName' | 'honorificPrefix' | 'honorificSuffix' | 'id' | 'ims' | 'locality' | 'middleName' | 'name' | 'nickname' | 'note' | 'organizations' | 'phoneNumbers' | 'photos' | 'postalCode' | 'region' | 'streetAddress' | 'title' | 'urls';

export interface IContactFindOptionsMock {
    filter?: string;
    multiple?: boolean;
    desiredFields?: string[];
    hasPhoneNumber?: boolean;
}

@Injectable()
export class ContactFindOptionsMock implements IContactFindOptionsMock {
    filter: string;
    multiple: boolean;
    desiredFields: string[];
    hasPhoneNumber: boolean;
    constructor(filter?: string, multiple?: boolean, desiredFields?: string[], hasPhoneNumber?: boolean) {}
}