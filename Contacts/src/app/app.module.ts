import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Contacts, Contact } from '@ionic-native/contacts';

import { SpeedDialPage } from '../pages/speeddial/speeddial';
import { RecentPage } from '../pages/recent/recent';
import { ContactsPage } from '../pages/contacts/contacts';
import { TabsPage } from '../pages/tabs/tabs';
import { AddContactPage } from '../pages/add-contact/add-contact';
import { ContactDetailPage } from '../pages/contact-detail/contact-detail';

import { ContactsMock } from '../mocks/contacts.mock';
import { ContactMock } from '../mocks/contact.mock';
import { CallService } from '../pages/services/call-service';
import { TestContacts } from '../pages/services/test-contacts';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    SpeedDialPage,
    RecentPage,
    ContactsPage,
    TabsPage,
    AddContactPage,
    ContactDetailPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SpeedDialPage,
    RecentPage,
    ContactsPage,
    TabsPage,
    AddContactPage,
    ContactDetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    CallService,
    TestContacts,
    Contacts,
    Contact,
    ContactMock,
    ContactsMock,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
