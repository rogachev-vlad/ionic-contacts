import { Injectable } from '@angular/core';

@Injectable()
export class TestContacts
{
    getTestContacts(): any[]
    {
        return [{
            displayName: 'John Snow',
            nickName: 'Know nothing',
            phoneNumber: '88005553535',
            phoneType: 'Mobile'
        }, {
            displayName: 'Arja Stark',
            nickName: 'Arja',
            phoneNumber: '88005553535',
            phoneType: 'Mobile'
        }, {
            displayName: 'Daenerys Targaryen',
            nickName: 'Mother of dragons',
            phoneNumber: '88005553535',
            phoneType: 'Mobile'
        }, {
            displayName: 'Tyrion Lannister',
            nickName: 'The imp',
            phoneNumber: '88005553535',
            phoneType: 'Mobile'
        }, {
            displayName: 'Cersei Lannister',
            nickName: 'Mother',
            phoneNumber: '88005553535',
            phoneType: 'Mobile'
        }, {
            displayName: 'Melisandre',
            nickName: 'Red woman',
            phoneNumber: '88005553535',
            phoneType: 'Mobile'
        }, {
            displayName: 'Khal Drogo',
            nickName: 'Dothrakian',
            phoneNumber: '88005553535',
            phoneType: 'Mobile'
        }, {
            displayName: 'Davos Seaworth',
            nickName: 'Onion Knight',
            phoneNumber: '88005553535',
            phoneType: 'Mobile'
        }, {
            displayName: 'Petyr Baelish',
            nickName: 'Littlefinger',
            phoneNumber: '88005553535',
            phoneType: 'Mobile'
        }]
    }
}