import { Injectable } from '@angular/core';

declare var window;

@Injectable()
export class CallService
{

    constructor() {}

    callNumber(passedNumber)
    {
        passedNumber = encodeURIComponent(passedNumber);
        window.location = "tel:"+passedNumber;
    }
}