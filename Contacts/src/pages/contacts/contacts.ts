import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Content, NavController } from 'ionic-angular';

import { CallService } from '../services/call-service';
import { ContactMock } from '../../mocks/contact.mock';
import { ContactsMock, ContactFieldTypeMock, ContactFindOptionsMock } from '../../mocks/contacts.mock';

import { AddContactPage } from '../add-contact/add-contact';
import { ContactDetailPage } from '../contact-detail/contact-detail';

@Component({
  selector: 'page-contacts',
  templateUrl: 'contacts.html'
})
export class ContactsPage
{
    contactsFound = [];
	public search: boolean = false;
    public visible: boolean = true; // "visible" doesn't update in .html

    @ViewChild('content') content: Content;

	ionViewDidEnter()
    {
        console.log('ionViewDidEnter Contacts');
	}

    ionViewDidLoad()
    {
		this.findContact('');
        this.content.ionScrollStart.subscribe((scroll) => {
            this.visible = false;
            this.detector.detectChanges();
        });
        this.content.ionScrollEnd.subscribe((scroll) => {
            this.visible = true;
            this.detector.detectChanges();
            //console.log('user scrolled', scroll.directionY, this.visible); // log from changing (here is ok)
        });
        console.log('ionViewDidLoad Contacts');
    }

    constructor(public navCtrl: NavController, private contacts: ContactsMock, private callService: CallService,
        private detector: ChangeDetectorRef) {}

	newContact() {
		this.navCtrl.push(AddContactPage);
	}

	contactDetail(contact:ContactMock)
    {
		this.navCtrl.push(ContactDetailPage, { contact: contact });
	}

    findContact(ev:any)
    {
		let fields:ContactFieldTypeMock[] = ['displayName'];

		const options = new ContactFindOptionsMock();
		options.filter = ev;
		options.multiple = true;
		options.hasPhoneNumber = true;

		this.contacts.find(fields, options).then((allContacts) => {
			this.contactsFound = allContacts;
		    this.search = this.contactsFound.length > 0 ? true : false;
		});
	}

    remove(contact:ContactMock)
    {
        for(var i = 0; i < this.contactsFound.length; i++)
        {
            if(this.contactsFound[i] == contact)
            {
                this.contactsFound.splice(i, 1);
            }
        }
        this.search = this.contactsFound.length > 0 ? true : false;
        this.visible = true;
        this.detector.detectChanges();
        console.log('user removed', this.visible);
        contact.remove();
    }

    callNumber(passedNumber)
    {
        this.callService.callNumber(passedNumber);
    }
}
