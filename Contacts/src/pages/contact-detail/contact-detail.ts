import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CallService } from '../services/call-service';
import { ContactMock } from '../../mocks/contact.mock';

/**
 * Generated class for the ContactDetail page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-contact-detail',
    templateUrl: 'contact-detail.html',
})
export class ContactDetailPage
{
    public contact: ContactMock;

    constructor(public navCtrl: NavController, public navParams: NavParams, private callService: CallService)
    {
        this.contact = navParams.get("contact");
    }

    ionViewDidLoad()
    {
        console.log('ionViewDidLoad ContactDetail');
    }

    callNumber(passedNumber)
    {
        this.callService.callNumber(passedNumber);
    }
}
