import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ContactMock, ContactFieldMock } from '../../mocks/contact.mock';
import { ContactsMock } from '../../mocks/contacts.mock';
import { ContactDetailPage } from '../contact-detail/contact-detail';

/**
 * Generated class for the Addcontact page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-contact',
  templateUrl: 'add-contact.html',
})
export class AddContactPage
{
    contactObject = {
        displayName: '',
        nickName: '',
        phoneNumber: '',
        phoneType: ''
    }

    constructor(public navCtrl: NavController, public navParams: NavParams, private contacts: ContactsMock) {}

    ionViewDidLoad()
    {
        console.log('ionViewDidLoad Addcontact');
    }

    addContact(newContact:any)
    {
        let contact: ContactMock = this.contacts.create();
        contact.displayName = newContact.displayName;
        contact.nickname = newContact.nickName;

        let contactField: ContactFieldMock = new ContactFieldMock();
        contactField.type = newContact.phoneType;
        contactField.value = newContact.phoneNumber;
        contactField.pref = true;

        var numberSection = [];
        numberSection.push(contactField);
        contact.phoneNumbers = numberSection;

        contact.save().then((value) => {
            this.navCtrl.pop();
            this.navCtrl.push(ContactDetailPage, { contact: contact });
        }, (error) => {
            console.log(error);
        })

    }
}
