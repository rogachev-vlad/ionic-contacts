import { Component } from '@angular/core';

import { SpeedDialPage } from '../speeddial/speeddial';
import { RecentPage } from '../recent/recent';
import { ContactsPage } from '../contacts/contacts';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = SpeedDialPage;
  tab2Root = RecentPage;
  tab3Root = ContactsPage;

  constructor() {

  }
}
