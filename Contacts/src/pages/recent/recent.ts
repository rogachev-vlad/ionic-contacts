import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-recent',
  templateUrl: 'recent.html'
})
export class RecentPage {

  constructor(public navCtrl: NavController) {

  }

}
